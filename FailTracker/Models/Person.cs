﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FailTracker.Models
{
    public class Person: Entity
    {
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }

    }

    public abstract class Entity
    {
        public int Id { get; set; }
    }
}
