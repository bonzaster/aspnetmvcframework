﻿using System.Web.Optimization;

namespace FailTracker
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/jquery/js")
                .Include("~/Scripts/jquery-1.9.1.js"));

            bundles.Add(new ScriptBundle("~/bootstrap/js")
                .Include("~/Scripts/bootstrap.js")
                .Include("~/Scripts/bootstrap-datepicker.js")
                );

            bundles.Add(new StyleBundle("~/bootstrap/css")
                    .Include("~/Content/bootstrap.css"));
        }
    }
}