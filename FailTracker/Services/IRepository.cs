﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FailTracker.Models;
using FailTracker.ViewModels;

namespace FailTracker.Services
{
    public interface IRepository<T>: IDisposable
    {
        T Find(int id);
        IEnumerable<T> GetAll();

        void Add(T t);
        void Update(T t);
        void Delete(int id);
    }

    public class PersonRepository : IRepository<PersonDto>
    {
        private readonly FailTrackerContext _db;

        public PersonRepository(FailTrackerContext db)
        {
            _db = db;
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Person, PersonDto>();
            });
        }

        public PersonDto Find(int id)
        {
            return Mapper.Map<Person, PersonDto>(_db.People.Find(id));
        }

        public IEnumerable<PersonDto> GetAll()
        {
           return Mapper.Map<IList<Person>, IList<PersonDto>>(_db.People.ToList());
        }

        public void Add(PersonDto person)
        {
            _db.People.Add(new Person
            {
                BirthDate = person.BirthDate, Name = person.Name
            });
            _db.SaveChanges();
        }

        public void Update(PersonDto personDto)
        {
            var person = _db.People.Find(personDto.Id);
            person.BirthDate = personDto.BirthDate;
            person.Name = personDto.Name;
            _db.Entry(person).State = EntityState.Modified;
            _db.SaveChanges();
        }

        public void Delete(int id)
        {
            var person = _db.People.Find(id);
            _db.People.Remove(person);
            _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
